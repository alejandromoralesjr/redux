### Flux is a design Pattern
The Flux design pattern is made up of four parts, organized as one-way data pipeline. 
The **view** dispaches **actions** that <u>describe</u> what happened. The **store** receives these **actions** and determines what state changes should occur. After the state updates, the new state is pushed to the View. 

### Flux benefits: 
* Breaking up state management logic. 
  *  As parts of the state tree become interdependent, most of an app's state usually gets rolled up to a top-level component. Flux relieves the top-level components of state managements reponsibility and allows you to break up state management into isolated, smaller, and testable parts. 
*  React components are simpler. 
   *  Certain componets-managed state is fine, React components become simple HTML rendering functions.    

### Redux
Key ideas: 
*  All of your application's data is in a single data structure called the state which is held in the store. 
*  Your app reads the state from this store. 
*  The views emit actions that describe what happened. 
*  A new state is created by combining the old state and the action by a function called the reducer. 